# docker running cpp code
## Abstract:
Running code natively on a computer is the common way to develop and execute programs. At some point it's even better if the environment is a container. Indeed when the container executes the code, the environment is isolated from the host. As such there is less risk for collision with other projects and conflicts. In short less likely for a regression to occur.

## Run code natively:
### Pre-requisites:
- having g++/gcc installed and available as an easily executable binary
### script:
```shell
g++ -o HelloWorld HelloWorld.cpp
./HelloWorld
```
### output:
The console prints in the shell the standard output.

## Setup:
### Requirements:
* Docker
* An OS compatible with Linux Kernel for Docker (or has api to run docker)

### Quality of Life stuff
run docker in privileged mode by default
`sudo usermod -aG docker $USER`
So docker can be summoned without explicitely using extra powers.

### Script:
```bash
docker -v
docker build -t cpp-code:v1 .
docker run -it cpp-code:v1
```

### sources
- [docker](https://www.codeguru.com/cplusplus/using-c-with-docker-engine/)
- [tests](https://gitlab.com/mklimenko29/ci_example/-/blob/master/.gitlab-ci.yml)