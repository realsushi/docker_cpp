FROM gcc:12.1
COPY . /HelloWorld
WORKDIR /HelloWorld
RUN g++ -o HelloWorld HelloWorld.cpp
CMD ["./HelloWorld"]